package br.com.devmedia.conversores;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;


import br.com.devmedia.beans.Setor;
import br.com.devmedia.jpa.EntityManagerUtil;

public class ConverterSetor implements Converter,Serializable{

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String string) {
		if (string == null || string.equals("Selecione um Setor")){
			return null;
		}
		return EntityManagerUtil.getEntityManager().find(Setor.class,Integer.parseInt(string) );
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object o) {
		if( o == null){
			return null;
		}
		Setor obj = (Setor) o;
		return obj.getId().toString();
		
	}

}
