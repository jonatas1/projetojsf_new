package br.com.devmedia.teste;


import java.util.List;

import br.com.devmedia.beans.Setor;
import br.com.devmedia.modelo.DAOSetor;

public class TesteDaoGenerico {

	public static void main(String[] args) {
		DAOSetor<Setor> dao = new DAOSetor<Setor>();
		List<Setor> lista = dao.listar();
		dao.setMaximoObjetos(1);
		for(Setor o : lista){
			System.out.println("codigo: "+o.getId()+"Nome: "+o.getNome());
		}

	}

}
