package br.com.devmedia.teste;

import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.devmedia.beans.Funcionario;
import br.com.devmedia.beans.Grupo;
import br.com.devmedia.beans.Setor;
import br.com.devmedia.jpa.EntityManagerUtil;

public class TesteInserirFuncionario {

	public static void main(String[] args) {
		
		EntityManager em = EntityManagerUtil.getEntityManager();
		Grupo grupo = em.find(Grupo.class, 1);
		Setor setor = em.find(Setor.class, 1);
		Funcionario f = new Funcionario();
		f.setAtivo(true);
		f.setCpf("483.852.822-11");
		f.setEmail("jsovml@gmail.com");
		f.setGrupo(grupo);
		f.setNascimento(Calendar.getInstance());
		f.setNome("Joao");
		f.setNomeUsuario("joao");
		f.setSalario(5000.00);
		f.setSenha("12345");
		f.setSetor(setor);
		
		em.getTransaction().begin();
		em.persist(f);
		em.getTransaction().commit();
		
	}

}
